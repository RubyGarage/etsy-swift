//
//  ImageHeaderView.swift
//  Etsy
//
//  Created by Radislav Crechet on 4/10/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit

class ImageHeaderView: UIView {

    // MARK: Outlets
    
    @IBOutlet var imageView: UIImageView!

}
