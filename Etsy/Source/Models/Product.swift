//
//  Product.swift
//  Etsy
//
//  Created by Radislav Crechet on 4/6/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Product: Object, Mappable {
    
    // MARK: Properties
    
    dynamic var id = 0
    dynamic var thumbnailImageUrlString = ""
    dynamic var originalImageUrlString = ""
    dynamic var title = ""
    dynamic var price = ""
    dynamic var detail = ""
    
    // MARK: Configuration
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: Mappable
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map[Constants.NetworkKeys.idOfProduct]
        thumbnailImageUrlString <- map[Constants.NetworkKeys.thumbnailImageOfProduct]
        originalImageUrlString <- map[Constants.NetworkKeys.thumbnailImageOfProduct]
        title <- map[Constants.NetworkKeys.titleOfProduct]
        price <- map[Constants.NetworkKeys.priceOfProduct]
        detail <- map[Constants.NetworkKeys.detailOfProduct]
    }
    
}
