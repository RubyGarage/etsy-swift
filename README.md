# README #

### MVVM application that used: ###

* Closure for bind View with View Model
* Etsy API for load products from Etsy shop database
* Alamofire pod for work with network
* AlamofireImage pod for load and cache images
* RealmSwift pod for work with Realm database
* ObjectMapper pod for map network responses from JSON to models